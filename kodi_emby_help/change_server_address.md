# Changer l'adresse du serveur Emby
### Aller dans les paramètres :
![](./main.png)
###  Puis dans paramètres système :
![](./settings.png)
### Aller sur **Extensions** (1) Changer la vue en **Avancé** ou **Expert** (2) puis aller dans **En cours d'éxécution** (3) :
![](./system_settings.png)
### Faire apparaître le menu des options en laissant appuyer le bouton de sélection sur **Emby** :
![](./running_addons_1.png)
### Aller dans **Paramètres** :
![](./running_addons_2.png)
### Changer l'**Adresse principale du serveur** avec la nouvelle adresse :
![](./emby_settings.png)